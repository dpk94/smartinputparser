package dpk94icu.com.au.smartinputparser.MessageParser;

import android.util.Log;
import android.webkit.URLUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

/**
 * Created by dpk94icu on 17/12/2015.
 */
public class LinksParser extends ParserImpl{

    final String TAG = "LinksParser";
    final String TITLE = "links";


    @Override
    public void parseMessage(ParserSequentialImpl parser, int indexOfParsers, String[] inputs) throws JSONException {

        this.inputs = inputs;
        this.parser = parser;
        this.indexOfParsers = indexOfParsers;

        for (int i = 0 ; i < inputs.length ; i++)
        {
            String input = inputs[i];

            try {
                Log.d(TAG, "input.substring(0, 8) " + input.substring(0, 8));
                Log.d(TAG, "input.substring(0, 7) " + input.substring(0, 7));

                if (input.substring(0, 8).equals("https://") || input.substring(0,7).equals("http://"))
                {
                    foundKeywords.add(input);
                   // String host = "https://twitter.com/jdorfman/status/430511497475670016";

                }

            }catch (Exception ix)
            {
                Log.d(TAG, "no HTTPS or HTTP");
            }

        }

        if (foundKeywords.size() == 0)
        {
            parser.parseCompleted(indexOfParsers, false, null, null);
        }else
        {
            for (String url : foundKeywords)
            {
                getContents(url);
            }
        }


    }

    public void getContents(final String host){


        new Thread(new Runnable(){
            @Override
            public void run() {

                String response = "";

                try {

                    Log.d(TAG, "HOST : " + host);
                    URL url = new URL(host);
                    URLConnection conn = null;
                    if (URLUtil.isHttpsUrl(host))
                    {
                        conn = url.openConnection();
                        ((HttpsURLConnection)conn).setHostnameVerifier(hv);
                    }else
                    {
                        conn = url.openConnection();
                    }

                    conn.setReadTimeout(15000);
                    conn.setConnectTimeout(15000);
                    conn.addRequestProperty("Content-Type", "text/html;charset=UTF-8");
                    conn.setRequestProperty("Accept-Charset", "utf-8");
                    conn.connect();

                    String line;
                    BufferedReader br=new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    int startInx = -1, finishIndex = -1;
                    while ((line=br.readLine()) != null) {

                        response+=line;
                        Log.d(TAG, "response : " + response);

                        if (line.toLowerCase().contains("</title>"))
                        {
                            finishIndex = response.indexOf("</title>");
                        }

                        if (line.toLowerCase().contains("<title>"))
                        {
                            startInx = response.indexOf("<title>") + 7;
                        }

                        if (startInx > 0 && finishIndex > 0)
                            break;
                    }

                    String readableText = Jsoup.parse(response.substring(startInx, finishIndex)).text();
                    Log.d(TAG, "title : " + readableText);


                    JSONObject urlObj = new JSONObject();

                    urlObj.put("url", host);
                    urlObj.put("title", readableText);
                    convertedContentsArray.put(urlObj);


                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d(TAG, "title : ");


                }finally {

                    indexOfCompleted++;

                    if (indexOfCompleted == foundKeywords.size())
                    {
                        parser.parseCompleted(indexOfParsers, true, TITLE, convertedContentsArray);
                    }

                }

            }
        }).start();

    }

    final HostnameVerifier hv=new HostnameVerifier(){
        public boolean verify(    final String arg0,    final SSLSession arg1){
            return true;
        }
    };

}
