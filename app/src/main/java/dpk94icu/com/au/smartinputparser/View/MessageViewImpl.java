package dpk94icu.com.au.smartinputparser.View;

/**
 * Created by dpk94icu on 17/12/2015.
 */


// Interface to display the converted json format from input data

public interface MessageViewImpl {

    public void receiveParsedMessage(String message);
}
