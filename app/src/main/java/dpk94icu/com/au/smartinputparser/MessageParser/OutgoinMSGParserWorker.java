package dpk94icu.com.au.smartinputparser.MessageParser;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import dpk94icu.com.au.smartinputparser.Config.AppConstant;
import dpk94icu.com.au.smartinputparser.View.MessageViewImpl;

/**
 * Created by dpk94icu on 17/12/2015.
 */
public class OutgoinMSGParserWorker extends AsyncTask implements ParserSequentialImpl {

    private String TAG = "OutgoinMSGParserWorker";
    MessageViewImpl messageView;
    int indexOfParser;
    String originalMessage;
    String[] originalSplitedMessage;
    String finalConvertedMessage = "{}";

    public OutgoinMSGParserWorker(MessageViewImpl messageView, String originalMessage) {

        this.messageView = messageView;
        this.originalMessage = originalMessage;

   }

    private void processRawMessage(Class<ParserImpl> parser)
    {
        try {
            Log.d(TAG, "OutgoinMSGParserWorker");
            ParserImpl parserInc = parser.newInstance();
            originalSplitedMessage = originalMessage.replaceAll("\\s+", " ").trim().split(" ");

            Log.d("TAG", "processRawMessage indexOfParser : " + indexOfParser);


            parserInc.parseMessage(this, indexOfParser, originalSplitedMessage);

        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void parseCompleted(int index, boolean status, String key, JSONArray convertedMessage) {

        Log.d (TAG, "parseCompleted : " + index);
        Log.d (TAG, "finalConvertedMessage : " + finalConvertedMessage);
        if (!status)  // no keyword which is relevant to the parser defined in the config
        {
            ;
        }
        else if (convertedMessage == null || convertedMessage.length() == 0) // seems it has keyword but something happend due to the network or jsonparser.
        {
            ;
        }else
        {
            try {

                JSONObject finalConvertedJson = new JSONObject(finalConvertedMessage);
                finalConvertedJson.put(key, convertedMessage);

                Log.d(TAG, finalConvertedJson.toString());
                finalConvertedMessage = finalConvertedJson.toString();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        indexOfParser++;

        if (indexOfParser == AppConstant.parsersOutgoing.length)
        {
            messageView.receiveParsedMessage(finalConvertedMessage.toString());
        }else
        {
            processRawMessage(AppConstant.parsersOutgoing[indexOfParser]);
        }


    }

    @Override
    protected Object doInBackground(Object[] objects) {

        indexOfParser = 0;
        if (AppConstant.parsersOutgoing.length > 0)
        {
            processRawMessage(AppConstant.parsersOutgoing[indexOfParser]);

        }

        return null;
    }
}
