package dpk94icu.com.au.smartinputparser.Config;

import dpk94icu.com.au.smartinputparser.MessageParser.EmoticonsParser;
import dpk94icu.com.au.smartinputparser.MessageParser.LinksParser;
import dpk94icu.com.au.smartinputparser.MessageParser.MentionsParser;
import dpk94icu.com.au.smartinputparser.MessageParser.ParserImpl;

/**
 * Created by dpk94icu on 17/12/2015.
 */

// Main idea is to use Strategy pattern with different states (Mentions, Links and Emoticons).
// We can easily inject other chat message type throught the AppConstant.

public class AppConstant {

    public static Class<ParserImpl>[] parsersIncoming = new Class[]{MentionsParser.class, LinksParser.class};
    public static Class<ParserImpl>[] parsersOutgoing = new Class[]{MentionsParser.class, LinksParser.class, EmoticonsParser.class};

    public static String[] testMessage = {
            "@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016",
            "Olympics are starting soon; http://www.nbcolympics.com",
            "Good morning! (megusta) (coffee)",
            "@chris you around?"
    };

}
