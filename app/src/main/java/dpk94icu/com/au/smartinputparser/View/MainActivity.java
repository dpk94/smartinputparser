package dpk94icu.com.au.smartinputparser.View;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

import dpk94icu.com.au.smartinputparser.Config.AppConstant;
import dpk94icu.com.au.smartinputparser.Controller.MessageParserManager;
import dpk94icu.com.au.smartinputparser.R;

public class MainActivity extends AppCompatActivity implements MessageViewImpl {

    TextView messageText;
    Handler handler = new Handler();
    EditText chat_edit;
    int cnt = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        messageText = (TextView) findViewById(R.id.message);
        chat_edit = (EditText) findViewById(R.id.chat_edit);

    }

    public void testMessage(View view)
    {
        chat_edit.setText(AppConstant.testMessage[cnt%AppConstant.testMessage.length]);
        cnt++;
    }

    public void sendMessage (View view)
    {
        MessageParserManager.getInstance().parseMessage(true, this, chat_edit.getText().toString());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto", "", null));
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "From Dongpil Kwak");
            emailIntent.putExtra(Intent.EXTRA_TEXT, messageText.getText().toString());
            startActivity(Intent.createChooser(emailIntent, "Send email..."));

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void receiveParsedMessage(final String message) {

        Log.d("MESSAGE", message);
        handler.post(new Runnable() {
            @Override
            public void run() {

                messageText.setText(message);
                chat_edit.setText("");

            }
        });


    }
}
