package dpk94icu.com.au.smartinputparser.Controller;

import android.widget.TextView;

import dpk94icu.com.au.smartinputparser.Config.AppConstant;
import dpk94icu.com.au.smartinputparser.MessageParser.OutgoinMSGParserWorker;
import dpk94icu.com.au.smartinputparser.MessageParser.ParserImpl;
import dpk94icu.com.au.smartinputparser.View.MessageViewImpl;

/**
 * Created by dpk94icu on 17/12/2015.
 */


// Additinoal message process (incoming/outgoing) can be added in the higher wrapper.

public class MessageParserManager {

    private static MessageParserManager parserManager;

    private MessageParserManager() {}

    public static synchronized MessageParserManager getInstance()
    {
        if (parserManager == null)
        {
            parserManager = new MessageParserManager();
        }

        return parserManager;
    }


    public void parseMessage(boolean isOutgoing, MessageViewImpl messageView, String message)
    {
        if (isOutgoing)
        {

            new OutgoinMSGParserWorker(messageView, message).execute();

        }else
        {
            ;
        }


    }




}
