package dpk94icu.com.au.smartinputparser.MessageParser;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by dpk94icu on 17/12/2015.
 */

// Main idea is to use Strategy pattern with different states (Mentions, Links and Emoticons).
// We can easily inject other chat message type throught the AppConstant.

public abstract class ParserImpl {

    ParserSequentialImpl parser;
    String[] inputs;
    int indexOfParsers, indexOfCompleted;
    ArrayList<String> foundKeywords = new ArrayList<String>();
    JSONArray convertedContentsArray = new JSONArray();

    abstract void parseMessage(ParserSequentialImpl parser, int index, String[] input) throws JSONException;

}
