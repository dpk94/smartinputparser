package dpk94icu.com.au.smartinputparser.MessageParser;

import android.util.Log;

import org.json.JSONException;

/**
 * Created by dpk94icu on 17/12/2015.
 */
public class MentionsParser  extends ParserImpl{

    final String TAG = "MentionsParser";
    final String TITLE = "mentions";

    @Override
    public void parseMessage(ParserSequentialImpl parser, int index, String[] inputs) throws JSONException {


        this.inputs = inputs;
        this.parser = parser;
        this.indexOfParsers = indexOfParsers;

        for (int i = 0 ; i < inputs.length ; i++) {

            String input = inputs[i];
            Log.d(TAG, "parseMessage : " + input);

            try {

                if (input.substring(0, 1).equals("@"))
                {

                    Log.d(TAG, "found : " + input);
                    foundKeywords.add(input.substring(1, input.length()));
                }

            }catch (Exception ix)
            {
                Log.d(TAG, "not related to the mentions keyword " + input);
            }



        }


        if (foundKeywords.size() == 0)
        {
            parser.parseCompleted(indexOfParsers, false, null, null);
        }else
        {
            for (String keyword : foundKeywords)
            {
                getContents(keyword);
            }

            parser.parseCompleted(indexOfParsers, true, TITLE, convertedContentsArray);
        }

    }

    private void getContents(String keyword)
    {
        convertedContentsArray.put(keyword);
    }
}
