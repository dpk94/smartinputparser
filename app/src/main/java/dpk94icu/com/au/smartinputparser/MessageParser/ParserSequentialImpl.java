package dpk94icu.com.au.smartinputparser.MessageParser;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by dpk94icu on 17/12/2015.
 */

// Sequentially move to the next parser and adding the final jsonFormat.

public interface ParserSequentialImpl {

    public void parseCompleted(int index, boolean status, String key, JSONArray value);

}
